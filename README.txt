d.o.Confirm (doconfirm) module

SUMMARY

This module allows users to add their drupal.org userIDs to their profiles and
confirm them.

HOW?

In a pretty hackish manner, mind you:
- you add your d.o userID to your profile;
- the site gives you a code, e.g. '456adcaa6770a0af';
- you add it to your 'Interests' on d.o;
- upon waiting for a minute or two (because of caching), you click 'Confirm' on
  the site;
- the site scans your d.o profile... et voilà!
- (upon successful confirmation, you can remove the code from your d.o profile)

This module does not do automated scraping or cause excessive traffic, so it is
pretty much harmless.

WHY?

Because we can, perhaps.
Some day, drupal.org might start running an oAuth provider or whatever, and
this module will become obsolete. Until then, think of it as a proactive market
study - is there demand for something like that?

(EVENTUAL) ROADMAP

- allow adding username instead of userID?
- add some Rules integration ('d.o. account has been confirmed' event -
  essential to be able to award roles to users who have confirmed accounts);
- provide an option to override default username theming (d.o link instead of
  local one);
- (your feature request here)
