<?php

/**
 * @file
 * d.o.Confirm (doconfirm) module.
 */

/**
 * Implements hook_permission().
 */
function doconfirm_permission() {
  return array(
    'add do accounts' => array(
      'title' => t('Add and confirm drupal.org accounts'),
      'description' => t('Allows users to add drupal.org accounts to their profiles'),
    ),
    'view do accounts' => array(
      'title' => t('View drupal.org accounts on user profiles'),
      'description' => t("Allows users to view drupal.org accounts on other users' profiles"),
    ),
  );
}

/**
 * Implements hook_user_view().
 */
function doconfirm_user_view($account) {
  global $user;
  $do = doconfirm_get($account->uid);
  if (!empty($do['status']) && user_access('view do accounts')) {
    $account->content['drupalorg'] = array(
      '#type' => 'user_profile_category',
      '#weight' => 4,
      '#title' => t('Drupal.org'),
    );
    $account->content['drupalorg']['do'] = array(
      '#type' => 'user_profile_item',
      '#title' => t('d.o profile (confirmed)') . ':',
      '#markup' => l($do['doname'], 'http://drupal.org/user/' . $do['douid']),
    );
  }
  elseif ($account->uid == $user->uid && user_access('add do accounts')) {
    $account->content['drupalorg'] = array(
      '#type' => 'user_profile_category',
      '#weight' => 4,
      '#title' => t('Drupal.org'),
    );
    $douid = empty($do['douid']) ? NULL : $do['douid'];
    $account->content['drupalorg']['userid'] = drupal_get_form('doconfirm_enter', $douid);
    if (!empty($douid)) {
      $account->content['drupalorg']['confirm'] = drupal_get_form('doconfirm_confirm');
    }
  }
}

/**
 * Form builder - build 'Enter your d.o userID' form.
 *
 * @ingroup forms
 */
function doconfirm_enter($form, $form_state, $default = NULL) {
  $form = array(
    'douid' => array(
      '#type' => 'textfield',
      '#title' => t('Enter your drupal.org userID') . ':',
      '#default_value' => $default,
      '#required' => TRUE,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
  );
  return $form;
}

/**
 * Submit handler for doconfirm_enter().
 */
function doconfirm_enter_submit($form, $form_state) {
  global $user;
  db_merge('doconfirm_datum')
    ->key(array('uid' => $user->uid))
    ->fields(array('douid' => $form_state['values']['douid']))
    ->execute();
}

/**
 * Form builder - build 'Confirm your d.o userID' form.
 *
 * @ingroup forms
 */
function doconfirm_confirm() {
  global $user;
  $do = doconfirm_get($user->uid);
  $instructions = '<b>' . t('Confirm your drupal.org userID') . ':</b>';
  $instructions .= '<pre>' . doconfirm_code($user->uid) . '</pre>';
  $instructions .= t('Add the above code to your <a href="http://drupal.org/user/!douid/edit/Personal%20information#edit-profile-interest-wrapper" target=_blank>interests on drupal.org</a>, save there and then click <b>Confirm</b> here.<br />', array('!douid' => $do['douid']));
  $form = array(
    'instructions' => array(
      '#title' => t('Confirm your drupal.org userID') . ':',
      '#markup' => $instructions,
    ),
    'douid' => array(
      '#type' => 'value',
      '#value' => $do['douid'],
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Confirm'),
    ),
  );
  return $form;
}

/**
 * Validator for doconfirm_confirm().
 */
function doconfirm_confirm_validate($form, &$form_state) {
  global $user;
  $code = doconfirm_code($user->uid);
  $profile_url = 'http://drupal.org/user/' . $form_state['values']['douid'];
  $raw = drupal_http_request($profile_url);
  if ($raw->code != '200') {
    form_set_error('instructions', t('Unable to connect to drupal.org. Try later?'));
  }
  else {
    $codestring = '<a href="/profile/profile_interest/' . $code . '">' . $code . '</a>';
    if (strpos($raw->data, $codestring) === FALSE) {
      form_set_error('instructions', t('Code not found on drupal.org profile. Check userID here, code there and perhaps wait for a minute because of caching on d.o.'));
    }
    else {
      // Find d.o username and pass it to the submit handler.
      $form_state['storage']['doname'] = doconfirm_parse_name($raw->data);
    }
  }
}

/**
 * Submit handler for doconfirm_confirm().
 */
function doconfirm_confirm_submit($form, $form_state) {
  global $user;
  db_merge('doconfirm_datum')
    ->key(array('uid' => $user->uid))
    ->fields(array(
      'douid' => $form_state['values']['douid'],
      'doname' => $form_state['storage']['doname'],
      'status' => 1,
    ))
    ->execute();
  drupal_set_message(t('Congratulations, your d.o profile has been confirmed. You may now remove the code from your interests. :)'));
}

/**
 * Implements hook_field_extra_fields().
 *
 * Expose d.o element on 'Display fields' so that it can be dragged around.
 */
function doconfirm_field_extra_fields() {
  $extra['user']['user'] = array(
    'display' => array(
      'drupalorg' => array(
        'label' => t('Drupal.org'),
        'description' => t('d.o.Confirm module view element.'),
        'weight' => 4,
      ),
    ),
  );
  return $extra;
}

/**
 * Helper function - parse d.o username from profile page HTML.
 *
 * @param string $data
 *   Data as returned from drupal_http_request().
 */
function doconfirm_parse_name($data) {
  $start = strpos($data, '<h1 id="page-title"') + 34;
  if (!$start) {
    // Something has gone wrong.
    return FALSE;
  }
  else {
    $end = strpos($data, '</h1>', $start);
    $name = substr($data, $start, $end - $start);
    return $name;
  }
}

/**
 * Helper function - get d.o userID and username by local userID.
 *
 * @param int $uid
 *   User ID.
 */
function doconfirm_get($uid) {
  $do = db_select('doconfirm_datum', 'dd')
    ->fields('dd', array('douid', 'doname', 'status'))
    ->condition('uid', $uid)
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();
  return empty($do) ? array() : $do;
}

/**
 * Helper function - prepare confirmation code for a given local userID.
 *
 * @param int $uid
 *   User ID.
 */
function doconfirm_code($uid) {
  // 16 chars is mighty enough for most use cases. Make length (and hash algo)
  // configurable for paranoids out there?
  return substr(hash_hmac('sha256', $uid, drupal_get_private_key() . drupal_get_hash_salt()), 0, 16);
}
